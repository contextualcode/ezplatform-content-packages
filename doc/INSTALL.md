# eZ Platform Content Packages Installation

1. Install with composer.

    ```bash
    $ composer require contextualcode/ezplatform-content-packages
    ```

2. Add to the `registerBundles` method of AppKernel.php:
    ```php
    public function registerBundles()
    {
        $bundles = array(
            ...
            new ContextualCode\EzPlatformContentPackagesBundle\EzPlatformContentPackagesBundle(),
            ...
        );
    }
    ```

3. Add this to your `app/config/routing.yml`:

    ```yml
    contextual_code_content_packages_bundle:
        resource: "@EzPlatformContentPackagesBundle/Resources/config/routing.yml"
    ```


4. Run the migrations:

    - If you're using [Kaliop eZ-Migration Bundle](https://github.com/kaliop-uk/ezmigrationbundle)
        ```bash
        php bin/console kaliop:migration:migrate --path=vendor/contextualcode/ezplatform-content-packages/src/bundle/MigrationVersions/20191020101530_mysql_create_cc_content_packages_status_table.sql
        ```

    - If you're using [DoctrineMigrationsBundle](https://github.com/doctrine/DoctrineMigrationsBundle)
        ```bash
        php bin/console doctrine:migrations:migrate --configuration=vendor/contextualcode/ezplatform-content-packages/src/bundle/Resources/config/doctrine_migrations.yml
        ```

    - If you're not using any migration bundle, execute SQL manually
        ```bash
        mysql < vendor/contextualcode/ezplatform-content-packages/src/bundle/MigrationVersions/20191020101530_mysql_create_cc_content_packages_status_table.sql

### Configuration
There are some configuration values you can change.
Some information about each parameter and their defaults:
```yml
ez_platform_content_packages:

    # The relative path from your web directory
    # (or the absolute path)
    # to the project root directory
    project_root_dir: '..'

    # From the project root directory,
    # the relative path to the working directory
    # this bundle will use
    storage_dir: 'var/content_packages'

    # From the 'storage_dir',
    # the relative path to the working directory
    # this bundle will use for imports
    relative_import_dir: 'imports'

    # From the 'storage_dir',
    # the relative path to the working directory
    # this bundle will use for exports
    relative_export_dir: 'exports'

    # The php binary to use to run sub-processes
    php_command: 'php'

    # The path from your project root dir to
    # the Symfony console
    console_path: 'bin/console'

    # These are the default params for
    # the export subtree form, using the keys from ExportSubtreeCommand.
    default_export_subtree_params: null
    # Example:
    # default_export_subtree_params:
    #     exclude-attributes: keywords,geoarea,topics,industry,page
    #     extra-fields-location-id-references: config
    #     add-brackets-to-reference-fields: config

    # These are the default params for
    # the export content types form, using the keys from ExportContentTypesCommand.
    default_export_content_types_params: null

    # The string to pass to strtotime to determine if a
    # Content Package is stale and should be removed
    stale_time_diff: '-1 day'
```
