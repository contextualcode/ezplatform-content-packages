# eZ Platform Content Packages TODO
- Allow export/import of users, locations vs. content, etc
- Smart handling of different Content Types on export/import 
    - UI to handle this
- Better output for import 
- Option to follow and export references to locations/contents outside of the subtree being exported 
- Option to ignore publish validation or not
- Tooltips for form options
- Options for Languages
