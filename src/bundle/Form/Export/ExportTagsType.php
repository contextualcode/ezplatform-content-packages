<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Form\Export;

use ContextualCode\EzPlatformContentPackagesBundle\Command\ExportContentTypesCommand;
use ContextualCode\EzPlatformContentPackagesBundle\Command\ExportTagsCommand;
use eZ\Publish\API\Repository\ContentTypeService;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\Core\MVC\Symfony\RequestStackAware;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class ExportTagsType extends AbstractType
{
    use RequestStackAware;

    // see constructor
    protected $PARAM_DEFAULTS = [];

    /** @var ConfigResolverInterface */
    protected $configResolver;

    /** @var ContentTypeService */
    protected $contentTypeService;

    /** @var array */
    protected $contentTypeIdentifierToGroup = [];

    public function __construct(
        ConfigResolverInterface $configResolver,
        $config
    ) {
        $this->configResolver = $configResolver;
        $this->PARAM_DEFAULTS = [
            ExportTagsCommand::PARAM_PARENT_TAG_ID => null,
        ];
        $params = $config['default_export_tags_params'] ?? [];
        foreach ($params as $key => $param) {
            $params[str_replace('_', '-', $key)] = $param;
        }
        $this->PARAM_DEFAULTS = array_merge(
            $this->PARAM_DEFAULTS,
            $params
        );
    }

    protected function getData()
    {
        $request = $this->getCurrentRequest();

        return array_merge($this->PARAM_DEFAULTS, $request->query->all());
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $form->setData($this->getData());
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(ExportTagsCommand::PARAM_PARENT_TAG_ID, IntegerType::class, [
                'label' => ExportTagsCommand::PARAM_PARENT_TAG_ID_LABEL,
                'required' => true,
            ])
            ->add('generate', SubmitType::class, [
                'label' => 'Export Content Package',
                'attr' => ['class' => 'btn btn-primary'],
            ])
            ->setMethod('GET');
    }
}
