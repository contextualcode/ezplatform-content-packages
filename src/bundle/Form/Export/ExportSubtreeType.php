<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Form\Export;

use ContextualCode\EzPlatformContentPackagesBundle\Command\ExportSubtreeCommand;
use eZ\Publish\API\Repository\LanguageService;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use eZ\Publish\Core\MVC\Symfony\RequestStackAware;
use EzSystems\EzPlatformAdminUiBundle\Templating\Twig\UniversalDiscoveryExtension;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class ExportSubtreeType extends AbstractType
{
    use RequestStackAware;

    // see constructor
    protected $PARAM_DEFAULTS = [];

    /** @var ConfigResolverInterface */
    protected $configResolver;

    /** @var UniversalDiscoveryExtension */
    protected $udwExtension;

    /** @var LanguageService */
    protected $languageService;

    public function __construct(
        ConfigResolverInterface $configResolver,
        UniversalDiscoveryExtension $udwExtension,
        LanguageService $languageService,
        $config
    ) {
        $this->configResolver = $configResolver;
        $this->udwExtension = $udwExtension;
        $this->languageService = $languageService;
        $this->PARAM_DEFAULTS = [
            ExportSubtreeCommand::PARAM_SUBTREE_LOCATION_ID => null,
            ExportSubtreeCommand::PARAM_EXCLUDE_ATTRIBUTES => '',
            ExportSubtreeCommand::PARAM_PRESERVE_OBJECT_STATES => true,
            ExportSubtreeCommand::PARAM_PRESERVE_REMOTE_IDS => false,
            ExportSubtreeCommand::PARAM_PRESERVE_OWNER => true,
            ExportSubtreeCommand::PARAM_PRESERVE_PUBLISHED => true,
            ExportSubtreeCommand::PARAM_PRESERVE_MODIFIED => true,
            ExportSubtreeCommand::PARAM_PRESERVE_SECTION => true,
            ExportSubtreeCommand::PARAM_INCLUDE_FILES => true,
            ExportSubtreeCommand::PARAM_INCLUDE_FILE_REFERENCES => true,
        ];
        $params = $config['default_export_subtree_params'] ?? [];
        foreach ($params as $key => $param) {
            $params[str_replace('_', '-', $key)] = $param;
        }
        $this->PARAM_DEFAULTS = array_merge(
            $this->PARAM_DEFAULTS,
            $params
        );
    }

    protected function getData()
    {
        $request = $this->getCurrentRequest();

        return array_merge($this->PARAM_DEFAULTS, $request->query->all());
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);

        $form->setData($this->getData());
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('browse', ButtonType::class, [
                'label' => 'Browse',
                'attr' => [
                    'class' => 'btn--udw-browse-custom btn btn-secondary',
                    'data-udw-config' => $this->udwExtension->renderUniversalDiscoveryWidgetConfig('single', [
                        'type' => 'object_relation',
                        'starting_location_id' => 1,
                    ]),
                    'data-starting-location-id' => $this->configResolver->getParameter(
                        'universal_discovery_widget_module.default_location_id'
                    ),
                ],
            ])
            ->add(ExportSubtreeCommand::PARAM_SUBTREE_LOCATION_ID, IntegerType::class, [
                'label' => false,
                'required' => true,
            ])
            ->add(ExportSubtreeCommand::PARAM_EXCLUDE_ATTRIBUTES, TextType::class, [
                'label' => ExportSubtreeCommand::PARAM_EXCLUDE_ATTRIBUTES_LABEL,
                'required' => false,
            ])
            ->add(ExportSubtreeCommand::PARAM_EXTRA_FIELDS_LOCATION_ID, TextType::class, [
                'label' => ExportSubtreeCommand::PARAM_EXTRA_FIELDS_LOCATION_ID_LABEL,
                'required' => false,
            ])
            ->add(ExportSubtreeCommand::PARAM_EXTRA_FIELDS_CONTENT_ID, TextType::class, [
                'label' => ExportSubtreeCommand::PARAM_EXTRA_FIELDS_CONTENT_ID_LABEL,
                'required' => false,
            ])
            ->add(ExportSubtreeCommand::PARAM_ADD_BRACKETS_TO_REFERENCE_FIELDS, TextType::class, [
                'label' => ExportSubtreeCommand::PARAM_ADD_BRACKETS_TO_REFERENCE_FIELDS_LABEL,
                'required' => false,
            ])
            ->add(ExportSubtreeCommand::PARAM_INCLUDE_FILES, CheckboxType::class, [
                'label' => ExportSubtreeCommand::PARAM_INCLUDE_FILES_LABEL  . ':',
                'required' => false,
            ])
            ->add(ExportSubtreeCommand::PARAM_INCLUDE_FILE_REFERENCES, CheckboxType::class, [
                'label' => ExportSubtreeCommand::PARAM_INCLUDE_FILE_REFERENCES_LABEL . ':',
                'required' => false,
            ])
            ->add(ExportSubtreeCommand::PARAM_PRESERVE_REMOTE_IDS, CheckboxType::class, [
                'label' => ExportSubtreeCommand::PARAM_PRESERVE_REMOTE_IDS_LABEL . ':',
                'required' => false,
            ])
            ->add(ExportSubtreeCommand::PARAM_PRESERVE_OWNER, CheckboxType::class, [
                'label' => ExportSubtreeCommand::PARAM_PRESERVE_OWNER_LABEL . ':',
                'required' => false,
            ])
            ->add(ExportSubtreeCommand::PARAM_PRESERVE_PUBLISHED, CheckboxType::class, [
                'label' => ExportSubtreeCommand::PARAM_PRESERVE_PUBLISHED_LABEL . ':',
                'required' => false,
            ])
            ->add(ExportSubtreeCommand::PARAM_PRESERVE_MODIFIED, CheckboxType::class, [
                'label' => ExportSubtreeCommand::PARAM_PRESERVE_MODIFIED_LABEL . ':',
                'required' => false,
            ])
            ->add(ExportSubtreeCommand::PARAM_PRESERVE_SECTION, CheckboxType::class, [
                'label' => ExportSubtreeCommand::PARAM_PRESERVE_SECTION_LABEL . ':',
                'required' => false,
            ])
            ->add(ExportSubtreeCommand::PARAM_PRESERVE_OBJECT_STATES, CheckboxType::class, [
                'label' => ExportSubtreeCommand::PARAM_PRESERVE_OBJECT_STATES_LABEL . ':',
                'required' => false,
            ])
            ->add(ExportSubtreeCommand::PARAM_LANGUAGE, ChoiceType::class, [
                'label' => ExportSubtreeCommand::PARAM_LANGUAGE_LABEL . ':',
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'choices' => $this->loadLanguages(),
            ])
            ->add('generate', SubmitType::class, [
                'label' => 'Export Content Package',
                'attr' => ['class' => 'btn btn-primary'],
            ])
            ->setMethod('GET');
    }

    protected function loadLanguages()
    {
        $classes = [];
        $languages = $this->languageService->loadLanguages();
        foreach ($languages as $language) {
            $classes[$language->name] = $language->languageCode;
        } 
        return $classes;
    }
}
