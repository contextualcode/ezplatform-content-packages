<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * @ORM\Entity
 * @ORM\Table(name="cc_content_packages_status")
 */
class ContentPackageStatus
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="package_id")
     */
    protected $packageId;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", name="user_id")
     */
    protected $userId;

    /**
     * @ORM\Column(type="integer", name="timestamp")
     */
    protected $timestamp;

    /**
     * @var bool
     * @ORM\Column(type="boolean", name="in_progress")
     */
    protected $inProgress;

    /**
     * @ORM\Column(type="integer", name="progress")
     */
    protected $progress;

    /**
     * @ORM\Column(type="string", name="error")
     */
    protected $error;

    /**
     * return int
     */
    public function getId()
    {
        return $this->packageId . '-' . $this->userId;
    }

    /**
     * The date the report was last generated.
     * @return DateTime
     * @throws Exception
     */
    public function getDate()
    {
        if ($this->timestamp === null) {
            return null;
        }

        return (new DateTime())->setTimestamp($this->timestamp);
    }

    public function setDate($date)
    {
        if ($date instanceof DateTime) {
            $this->timestamp = $date->getTimestamp();
        } elseif ($date === null) {
            $this->timestamp = null;
        }
    }

    /**
     * Whether it is currently being generated or not
     * @return bool
     */
    public function isInProgress(): bool
    {
        return $this->inProgress;
    }

    public function setInProgress(bool $inProgress)
    {
        $this->inProgress = $inProgress;
    }

    /**
     * If in progress, an int from 0-99 representing progress.
     * @return int
     */
    public function getProgress(): int
    {
        return $this->progress;
    }

    public function setProgress(int $progress): void
    {
        $this->progress = $progress;
    }

    /**
     * @return string
     */
    public function getPackageId(): string
    {
        return $this->packageId;
    }

    public function setPackageId(string $packageId): void
    {
        $this->packageId = $packageId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }

    public function setError(string $error): void
    {
        $this->error = $error;
    }
}
