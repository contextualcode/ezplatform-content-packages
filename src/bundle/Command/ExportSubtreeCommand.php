<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Command;

use ContextualCode\EzPlatformContentPackagesBundle\Service\ContentPackagesExportService;
use eZ\Publish\API\Repository\Exceptions\NotFoundException;
use eZ\Publish\API\Repository\Repository;
use eZ\Publish\Core\Repository\Values\User\UserReference;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportSubtreeCommand extends ContainerAwareCommand
{
    public const COMMAND_NAME = 'contextual_code:content_packages:export_subtree';
    public const PARAM_SUBTREE_LOCATION_ID_LABEL = 'Location ID';
    public const PARAM_SUBTREE_LOCATION_ID = 'subtree-location-id';
    public const PARAM_LANGUAGE_LABEL = 'Language';
    public const PARAM_LANGUAGE = 'language';
    public const PARAM_PRESERVE_REMOTE_IDS_LABEL = 'Preserve Remote Ids';
    public const PARAM_PRESERVE_REMOTE_IDS = 'preserve-remote-ids';
    public const PARAM_PRESERVE_OWNER_LABEL = 'Preserve Owner';
    public const PARAM_PRESERVE_OWNER = 'preserve-owner';
    public const PARAM_PRESERVE_MODIFIED_LABEL = 'Preserve Modification Date';
    public const PARAM_PRESERVE_MODIFIED = 'preserve-modified';
    public const PARAM_PRESERVE_PUBLISHED_LABEL = 'Preserve Publication Date';
    public const PARAM_PRESERVE_PUBLISHED = 'preserve-published';
    public const PARAM_PRESERVE_SECTION_LABEL = 'Preserve Section';
    public const PARAM_PRESERVE_SECTION = 'preserve-section';
    public const PARAM_PRESERVE_OBJECT_STATES_LABEL = 'Preserve Object States';
    public const PARAM_PRESERVE_OBJECT_STATES = 'preserve-object-states';
    public const PARAM_INCLUDE_FILES_LABEL = 'Include File Binaries';
    public const PARAM_INCLUDE_FILES = 'include-files';
    public const PARAM_INCLUDE_FILE_REFERENCES_LABEL = 'Include File References';
    public const PARAM_INCLUDE_FILE_REFERENCES = 'include-file-references';
    public const PARAM_EXTRA_FIELDS_LOCATION_ID_LABEL = 'Attributes to Replace Hardcoded Location Ids In';
    public const PARAM_EXTRA_FIELDS_LOCATION_ID = 'extra-fields-location-id-references';
    public const PARAM_EXTRA_FIELDS_CONTENT_ID_LABEL = 'Attributes to Replace Hardcoded Content Ids In';
    public const PARAM_EXTRA_FIELDS_CONTENT_ID = 'extra-fields-content-id-references';
    public const PARAM_ADD_BRACKETS_TO_REFERENCE_FIELDS_LABEL = 'Add Brackets Around References in Fields';
    public const PARAM_ADD_BRACKETS_TO_REFERENCE_FIELDS = 'add-brackets-to-reference-fields';
    public const PARAM_ADMIN_LOGIN_LABEL = 'Admin Login';
    public const PARAM_ADMIN_LOGIN = 'admin-login';
    public const PARAM_EXCLUDE_ATTRIBUTES_LABEL = 'Exclude Fields from Export';
    public const PARAM_EXCLUDE_ATTRIBUTES = 'exclude-attributes';
    public const PARAM_PACKAGE_ID_LABEL = 'Package Id';
    public const PARAM_PACKAGE_ID = 'package-id';

    /** @var InputInterface|null */
    protected $input = null;
    /** @var OutputInterface|null */
    protected $output = null;

    /** @var ContentPackagesExportService */
    protected $exportService;
    /** @var Repository */
    private $repository;

    public function __construct(
        ContentPackagesExportService $exportService,
        Repository $repository
    ) {
        parent::__construct();
        $this->exportService = $exportService;
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription(
                'Generates a Content Package'
            )
            ->addOption(
                self::PARAM_SUBTREE_LOCATION_ID,
                null,
                InputOption::VALUE_REQUIRED,
                self::PARAM_EXTRA_FIELDS_LOCATION_ID_LABEL
            )
            ->addOption(
                self::PARAM_LANGUAGE,
                null,
                InputOption::VALUE_OPTIONAL,
                self::PARAM_LANGUAGE_LABEL
            )
            ->addOption(
                self::PARAM_ADMIN_LOGIN,
                null,
                InputOption::VALUE_REQUIRED,
                self::PARAM_ADMIN_LOGIN_LABEL
            )
            ->addOption(
                self::PARAM_EXCLUDE_ATTRIBUTES,
                null,
                InputOption::VALUE_OPTIONAL,
                self::PARAM_EXCLUDE_ATTRIBUTES_LABEL . ' (comma-separated, of the form "attribute_identifier or "content_type_identifier/attribute_identifier")',
                ''
            )
            ->addOption(
                self::PARAM_EXTRA_FIELDS_LOCATION_ID,
                null,
                InputOption::VALUE_OPTIONAL,
                self::PARAM_EXTRA_FIELDS_LOCATION_ID_LABEL . ' (comma-separated)',
                ''
            )
            ->addOption(
                self::PARAM_EXTRA_FIELDS_CONTENT_ID,
                null,
                InputOption::VALUE_OPTIONAL,
                self::PARAM_EXTRA_FIELDS_CONTENT_ID_LABEL . ' (comma-separated)',
                ''
            )
            ->addOption(
                self::PARAM_ADD_BRACKETS_TO_REFERENCE_FIELDS,
                null,
                InputOption::VALUE_OPTIONAL,
                self::PARAM_ADD_BRACKETS_TO_REFERENCE_FIELDS_LABEL . ' (comma-separated)',
                ''
            )
            ->addOption(
                self::PARAM_INCLUDE_FILES,
                null,
                InputOption::VALUE_NONE,
                self::PARAM_INCLUDE_FILES_LABEL
            )
            ->addOption(
                self::PARAM_INCLUDE_FILE_REFERENCES,
                null,
                InputOption::VALUE_NONE,
                self::PARAM_INCLUDE_FILE_REFERENCES_LABEL
            )
            ->addOption(
                self::PARAM_PRESERVE_REMOTE_IDS,
                null,
                InputOption::VALUE_NONE,
                self::PARAM_PRESERVE_REMOTE_IDS_LABEL
            )
            ->addOption(
                self::PARAM_PRESERVE_OWNER,
                null,
                InputOption::VALUE_NONE,
                self::PARAM_PRESERVE_OWNER_LABEL
            )
            ->addOption(
                self::PARAM_PRESERVE_MODIFIED,
                null,
                InputOption::VALUE_NONE,
                self::PARAM_PRESERVE_MODIFIED_LABEL
            )
            ->addOption(
                self::PARAM_PRESERVE_PUBLISHED,
                null,
                InputOption::VALUE_NONE,
                self::PARAM_PRESERVE_PUBLISHED_LABEL
            )
            ->addOption(
                self::PARAM_PRESERVE_SECTION,
                null,
                InputOption::VALUE_NONE,
                self::PARAM_PRESERVE_SECTION_LABEL
            )
            ->addOption(
                self::PARAM_PRESERVE_OBJECT_STATES,
                null,
                InputOption::VALUE_NONE,
                self::PARAM_PRESERVE_OBJECT_STATES_LABEL
            )
            ->addOption(
                self::PARAM_PACKAGE_ID,
                null,
                InputOption::VALUE_REQUIRED,
                self::PARAM_PACKAGE_ID_LABEL
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        $adminLogin = $this->input->getOption(self::PARAM_ADMIN_LOGIN);
        $locationId = $this->input->getOption(self::PARAM_SUBTREE_LOCATION_ID);
        $language = $this->input->getOption(self::PARAM_LANGUAGE);
        $excludeAttributes = $this->input->getOption(self::PARAM_EXCLUDE_ATTRIBUTES);
        $excludeAttributes = array_filter(explode(',', $excludeAttributes));
        $extraFieldsLocationId = $this->input->getOption(self::PARAM_EXTRA_FIELDS_LOCATION_ID);
        $extraFieldsLocationId = array_filter(explode(',', $extraFieldsLocationId));
        $extraFieldsContentId = $this->input->getOption(self::PARAM_EXTRA_FIELDS_CONTENT_ID);
        $extraFieldsContentId = array_filter(explode(',', $extraFieldsContentId));
        $addBracketsToFields = $this->input->getOption(self::PARAM_ADD_BRACKETS_TO_REFERENCE_FIELDS);
        $addBracketsToFields = array_filter(explode(',', trim($addBracketsToFields)));
        $includeFiles = (bool)$this->input->getOption(self::PARAM_INCLUDE_FILES);
        $includeFileReferences = (bool)$this->input->getOption(self::PARAM_INCLUDE_FILE_REFERENCES);
        $preserveRemoteIds = (bool)$this->input->getOption(self::PARAM_PRESERVE_REMOTE_IDS);
        $preserveOwner = (bool)$this->input->getOption(self::PARAM_PRESERVE_OWNER);
        $preservePublished = (bool)$this->input->getOption(self::PARAM_PRESERVE_PUBLISHED);
        $preserveModified = (bool)$this->input->getOption(self::PARAM_PRESERVE_MODIFIED);
        $preserveSection = (bool)$this->input->getOption(self::PARAM_PRESERVE_SECTION);
        $preserveObjectStates = (bool)$this->input->getOption(self::PARAM_PRESERVE_OBJECT_STATES);
        $packageId = $this->input->getOption(self::PARAM_PACKAGE_ID);

        $permissionResolver = $this->repository->getPermissionResolver();
        try {
            $permissionResolver->setCurrentUserReference(new UserReference(
                $this->repository->getUserService()->loadUserByLogin($adminLogin)->getUserId()
            ));
        } catch (NotFoundException $e) {
            $output->writeln($e->getMessage());

            return;
        }

        $this->exportService->setProjectRootDir('.');
        $this->exportService->exportSubtree(
            $adminLogin,
            $locationId,
            $packageId,
            $excludeAttributes,
            $extraFieldsLocationId,
            $extraFieldsContentId,
            $addBracketsToFields,
            $includeFiles,
            $includeFileReferences,
            $preserveRemoteIds,
            $preserveOwner,
            $preservePublished,
            $preserveModified,
            $preserveSection,
            $preserveObjectStates,
            $language,
            $output
        );
    }
}
