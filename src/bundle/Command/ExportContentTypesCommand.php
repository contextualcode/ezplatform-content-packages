<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Command;

use ContextualCode\EzPlatformContentPackagesBundle\Service\ContentPackagesExportService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportContentTypesCommand extends ContainerAwareCommand
{
    public static $COMMAND_NAME = 'contextual_code:content_packages:export_content_types';
    public const PARAM_CONTENT_TYPE_IDENTIFIERS_LABEL = 'Content Type identifiers';
    public const PARAM_CONTENT_TYPE_IDENTIFIERS = 'content-type-identifiers';
    public const PARAM_ADMIN_LOGIN_LABEL = 'Admin Login';
    public const PARAM_ADMIN_LOGIN = 'admin-login';
    public const PARAM_EXCLUDE_ATTRIBUTES_LABEL = 'Exclude Fields from Export';
    public const PARAM_EXCLUDE_ATTRIBUTES = 'exclude-attributes';
    public const PARAM_PACKAGE_ID_LABEL = 'Package Id';
    public const PARAM_PACKAGE_ID = 'package-id';
    public const PARAM_LANGUAGE_LABEL = 'Language';
    public const PARAM_LANGUAGE = 'language';

    /** @var InputInterface|null */
    protected $input = null;
    /** @var OutputInterface|null */
    protected $output = null;

    /** @var ContentPackagesExportService */
    protected $exportService;

    public function __construct(ContentPackagesExportService $exportService)
    {
        parent::__construct();
        $this->exportService = $exportService;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(self::$COMMAND_NAME)
            ->setDescription(
                'Generates a Content Package'
            )
            ->addOption(
                self::PARAM_CONTENT_TYPE_IDENTIFIERS,
                null,
                InputOption::VALUE_REQUIRED,
                self::PARAM_CONTENT_TYPE_IDENTIFIERS_LABEL . ' (comma-separated)',
                ''
            )
            ->addOption(
                self::PARAM_ADMIN_LOGIN,
                null,
                InputOption::VALUE_REQUIRED,
                self::PARAM_ADMIN_LOGIN_LABEL
            )
            ->addOption(
                self::PARAM_EXCLUDE_ATTRIBUTES,
                null,
                InputOption::VALUE_OPTIONAL,
                self::PARAM_EXCLUDE_ATTRIBUTES_LABEL . ' (comma-separated)',
                ''
            )
            ->addOption(
                self::PARAM_LANGUAGE,
                null,
                InputOption::VALUE_OPTIONAL,
                self::PARAM_LANGUAGE_LABEL,
                null
            )
            ->addOption(
                self::PARAM_PACKAGE_ID,
                null,
                InputOption::VALUE_REQUIRED,
                self::PARAM_PACKAGE_ID_LABEL
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        $adminLogin = $this->input->getOption(self::PARAM_ADMIN_LOGIN);
        $contentTypeIdentifiers = $this->input->getOption(self::PARAM_CONTENT_TYPE_IDENTIFIERS);
        $contentTypeIdentifiers = array_filter(explode(',', $contentTypeIdentifiers));
        $excludeAttributes = $this->input->getOption(self::PARAM_EXCLUDE_ATTRIBUTES);
        $excludeAttributes = array_filter(explode(',', $excludeAttributes));
        $packageId = $this->input->getOption(self::PARAM_PACKAGE_ID);
        $language = $this->input->getOption(self::PARAM_LANGUAGE);

        $this->exportService->setProjectRootDir('.');
        $this->exportService->exportContentTypes(
            $adminLogin,
            $packageId,
            $contentTypeIdentifiers,
            $excludeAttributes,
            $language,
            $output
        );
    }
}
