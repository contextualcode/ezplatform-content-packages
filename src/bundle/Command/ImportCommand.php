<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Command;

use ContextualCode\EzPlatformContentPackagesBundle\Service\ContentPackagesImportService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommand extends ContainerAwareCommand
{
    public const COMMAND_NAME = 'contextual_code:content_packages:import';
    public const PARAM_PARENT_ID_LABEL = ' ID of parent to import under (Location id for subtrees, or Tag id for Tags)';
    public const PARAM_PARENT_ID = 'parent-id';
    public const PARAM_ADMIN_LOGIN_LABEL = 'Admin Login';
    public const PARAM_TYPE_LABEL = 'Type of import';
    public const PARAM_TYPE = 'type';
    public const PARAM_ADMIN_LOGIN = 'admin-login';
    public const PARAM_PACKAGE_ID_LABEL = 'Package Id';
    public const PARAM_PACKAGE_ID = 'package-id';

    /** @var InputInterface|null */
    protected $input = null;
    /** @var OutputInterface|null */
    protected $output = null;

    /** @var ContentPackagesImportService */
    protected $importService;

    public function __construct(ContentPackagesImportService $importService)
    {
        parent::__construct();
        $this->importService = $importService;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription(
                'Imports a Content Package'
            )
            ->addOption(
                self::PARAM_PARENT_ID,
                null,
                InputOption::VALUE_OPTIONAL,
                self::PARAM_PARENT_ID_LABEL
            )
            ->addOption(
                self::PARAM_TYPE,
                null,
                InputOption::VALUE_REQUIRED,
                self::PARAM_TYPE_LABEL
            )
            ->addOption(
                self::PARAM_ADMIN_LOGIN,
                null,
                InputOption::VALUE_REQUIRED,
                self::PARAM_ADMIN_LOGIN_LABEL
            )
            ->addOption(
                self::PARAM_PACKAGE_ID,
                null,
                InputOption::VALUE_REQUIRED,
                self::PARAM_PACKAGE_ID_LABEL
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        $adminLogin = $this->input->getOption(self::PARAM_ADMIN_LOGIN);
        $parentId = $this->input->getOption(self::PARAM_PARENT_ID);
        $type = $this->input->getOption(self::PARAM_TYPE);
        $packageId = $this->input->getOption(self::PARAM_PACKAGE_ID);

        $this->importService->setProjectRootDir('.');
        $this->importService->import(
            $adminLogin,
            $packageId,
            $parentId,
            $type,
            $output
        );
    }
}
