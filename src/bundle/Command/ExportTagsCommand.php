<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Command;

use ContextualCode\EzPlatformContentPackagesBundle\Service\ContentPackagesExportService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportTagsCommand extends ContainerAwareCommand
{
    public static $COMMAND_NAME = 'contextual_code:content_packages:export_tags';
    public const PARAM_PARENT_TAG_ID_LABEL = 'Parent Tag ID';
    public const PARAM_PARENT_TAG_ID = 'parent-tag-id';
    public const PARAM_ADMIN_LOGIN_LABEL = 'Admin Login';
    public const PARAM_ADMIN_LOGIN = 'admin-login';
    public const PARAM_PACKAGE_ID_LABEL = 'Package Id';
    public const PARAM_PACKAGE_ID = 'package-id';

    /** @var InputInterface|null */
    protected $input = null;
    /** @var OutputInterface|null */
    protected $output = null;

    /** @var ContentPackagesExportService */
    protected $exportService;

    public function __construct(ContentPackagesExportService $exportService)
    {
        parent::__construct();
        $this->exportService = $exportService;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(self::$COMMAND_NAME)
            ->setDescription(
                'Generates a Content Package'
            )
            ->addOption(
                self::PARAM_PARENT_TAG_ID,
                null,
                InputOption::VALUE_REQUIRED,
                self::PARAM_PARENT_TAG_ID_LABEL
            )
            ->addOption(
                self::PARAM_ADMIN_LOGIN,
                null,
                InputOption::VALUE_REQUIRED,
                self::PARAM_ADMIN_LOGIN_LABEL
            )
            ->addOption(
                self::PARAM_PACKAGE_ID,
                null,
                InputOption::VALUE_REQUIRED,
                self::PARAM_PACKAGE_ID_LABEL
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
        $adminLogin = $this->input->getOption(self::PARAM_ADMIN_LOGIN);
        $parentTagId = $this->input->getOption(self::PARAM_PARENT_TAG_ID);
        $packageId = $this->input->getOption(self::PARAM_PACKAGE_ID);

        $this->exportService->setProjectRootDir('.');
        $this->exportService->exportTags(
            $adminLogin,
            $packageId,
            $parentTagId,
            $output
        );
    }
}
