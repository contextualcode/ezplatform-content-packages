<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('contextual_code_ez_platform_content_packages');

        $rootNode
            ->children()
                ->scalarNode('storage_dir')
                    ->defaultValue('var/content_packages')
                ->end()
                ->scalarNode('relative_import_dir')
                    ->defaultValue('imports')
                ->end()
                ->scalarNode('relative_export_dir')
                    ->defaultValue('exports')
                ->end()
                ->scalarNode('project_root_dir')
                    ->defaultValue('..')
                ->end()
                ->scalarNode('php_command')
                    ->defaultValue('php')
                ->end()
                ->scalarNode('console_path')
                    ->defaultValue('bin/console')
                ->end()
                ->scalarNode('stale_time_diff')
                    ->defaultValue('-1 day')
                ->end()
                ->scalarNode('metadata_date_format')
                    ->defaultValue('Y-m-d H:i:s')
                ->end()
                ->arrayNode('default_export_subtree_params')
                    ->useAttributeAsKey('name')
                    ->prototype('scalar')
                    ->end()
                ->end()
                ->arrayNode('default_export_content_types_params')
                    ->useAttributeAsKey('name')
                    ->prototype('scalar')
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
