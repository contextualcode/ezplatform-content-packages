<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Service;

use ContextualCode\EzPlatformContentPackagesBundle\Classes\ContentPackageMetadata;
use ContextualCode\EzPlatformContentPackagesBundle\Entity\ContentPackageStatus;
use ContextualCode\EzPlatformContentPackagesBundle\Service\Override\ContentService;
use DateTime;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Exception;
use eZ\Publish\API\Repository\ContentTypeService;
use eZ\Publish\API\Repository\LocationService;
use eZ\Publish\API\Repository\SearchService;
use Netgen\TagsBundle\Core\Repository\TagsService;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Yaml\Yaml;

class ContentPackagesService
{
    public const VERSION = '3.1.0';
    protected static $MIGRATION_FILE_DIRECTORY = 'var/content_packages';
    public static $PROJECT_ROOT_DIR = '..';
    public static $PHP_COMMAND = 'php';
    public static $CONSOLE_PATH = 'bin/console';
    public static $GENERATE_COMMAND = 'kaliop:migration:generate';
    public static $RELATIVE_IMPORTS_DIR = 'imports';
    public static $RELATIVE_EXPORTS_DIR = 'exports';
    public static $STALE_TIME_DIFF = '-1 day';

    public const SUBTREE_TYPE = 'subtree';
    public const CONTENT_TYPES_TYPE = 'content-types';
    public const TAGS_TYPE = 'tags';

    protected const ROOT_PLACEHOLDER_ID = 'ROOT_PLACEHOLDER';
    protected const LOG_FILENAME = 'status.log';
    public const METADATA_FILENAME = 'metadata.yml';
    public static $METADATA_DATE_FORMAT = 'Y-m-d H:i:s';

    protected const BATCH_LIMIT = 50;

    protected const ERROR_MESSAGES = [
        'log_write' => 'Log filepath \'$FILE\' not writable.',
        'command' => 'Command $COMMAND_INFO returned with return code \'$RETURN_CODE\'',
        'content_package' => 'Not a valid Content Package.',
        'content_package_missing' => 'Content package not found for user $ADMIN_LOGIN.',
        'package_id' => 'Please set a package id.',
        'parent_id' => 'Please set a parent id.',
        'entity' => 'Could not get entity.',
        'file_open' => 'Could not open file \'$FILEPATH\'',
    ];

    /** @var string|null */
    protected $adminLogin = null;
    /** @var string|null */
    protected $parentId = null;
    /** @var string|null */
    protected $type = null;
    /** @var string|null */
    protected $packageId = null;
    /** @var OutputInterface|null */
    protected $output = null;
    /** @var string|null */
    protected $language = null;

    /** @var SearchService */
    protected $searchService;

    /** @var LocationService */
    protected $locationService;

    /** @var ContentService */
    protected $contentService;

    /** @var ContentTypeService */
    protected $contentTypeService;

    /** @var EntityManager */
    protected $entityManager;

    /** @var ContentPackageStatus */
    protected $entity;

    /** @var EntityRepository */
    protected $statusRepository;

    /** @var KernelInterface */
    protected $kernel;

    /** @var TagsService */
    protected $tagsService;

    protected $config;

    public function __construct(
        $config,
        SearchService $searchService,
        LocationService $locationService,
        ContentService $contentService,
        ContentTypeService $contentTypeService,
        EntityManager $entityManager,
        KernelInterface $kernel
    ) {
        $this->config = $config;
        if (isset($config['storage_dir'])) {
            self::$MIGRATION_FILE_DIRECTORY = $config['storage_dir'];
        }
        if (isset($config['project_root_dir'])) {
            self::$PROJECT_ROOT_DIR = $config['project_root_dir'];
        }
        if (isset($config['php_command'])) {
            self::$PHP_COMMAND = $config['php_command'];
        }
        if (isset($config['console_path'])) {
            self::$CONSOLE_PATH = $config['console_path'];
        }
        if (isset($config['relative_imports_dir'])) {
            self::$RELATIVE_IMPORTS_DIR = $config['relative_imports_dir'];
        }
        if (isset($config['relative_exports_dir'])) {
            self::$RELATIVE_EXPORTS_DIR = $config['relative_exports_dir'];
        }
        if (isset($config['stale_time_diff'])) {
            self::$STALE_TIME_DIFF = $config['stale_time_diff'];
        }
        if (isset($config['metadata_date_format'])) {
            self::$METADATA_DATE_FORMAT = $config['metadata_date_format'];
        }

        $this->kernel = $kernel;
        $this->searchService = $searchService;
        $this->locationService = $locationService;
        $this->contentTypeService = $contentTypeService;
        $this->contentService = $contentService;
        $this->entityManager = $entityManager;
        $this->statusRepository = $entityManager->getRepository(ContentPackageStatus::class);
    }

    public function setTagsService(TagsService $tagsService)
    {
        $this->tagsService = $tagsService;
    }

    public function isTagsEnabled()
    {
        return $this->tagsService instanceof TagsService;
    }

    public function getEnabledTypes()
    {
        $types = [
            self::SUBTREE_TYPE => 'subtree',
            self::CONTENT_TYPES_TYPE => 'Content Types',
        ];
        if ($this->isTagsEnabled()) {
            $types[self::TAGS_TYPE] = 'Tags tree';
        }

        return $types;
    }

    protected function getCommandFailedError(
        string $commandInfo,
        int $returnCode
    ) {
        return str_replace(
            ['$COMMAND_INFO', '$RETURN_CODE'],
            [$commandInfo, $returnCode],
            self::ERROR_MESSAGES['command']
        );
    }

    protected function setCommandFailed(
        string $commandInfo,
        int $returnCode,
        ContentPackageStatus $entity
    ) {
        $entity->setInProgress(false);
        $entity->setError($this->getCommandFailedError($commandInfo, $returnCode));
        $this->persistStatus($entity);
    }

    public function getStatusLog(
        $adminLogin = null,
        $packageId = null,
        $noRootDir = false,
        $import = false
    ) {
        $logFilepath = $this->getLogFilepath($adminLogin, $packageId, $noRootDir, $import);

        return file_exists($logFilepath) ? file_get_contents($logFilepath) : '';
    }

    public function clearAndGetStatus(
        $adminLogin = null,
        $id = null
    ): ContentPackageStatus {
        $this->cleanupStaleContentPackages();

        if ($adminLogin === null) {
            $adminLogin = $this->getAdminLogin();
        }
        if ($id === null) {
            $id = $this->getPackageId();
        }

        try {
            $entity = $this->getStatus($adminLogin, $id);
        } catch (Exception $e) {
            $entity = new ContentPackageStatus();
        }
        $entity->setUserId($adminLogin);
        $entity->setPackageId($id);
        $entity->setDate(new DateTime());
        $entity->setProgress(0);
        $entity->setInProgress(false);
        $entity->setError('');
        $this->persistStatus($entity);
        $this->entity = $entity;

        return $entity;
    }

    public function cleanupStaleContentPackages()
    {
        $stalePackages = $this->getAllStalePackages();
        foreach ($stalePackages as $stalePackage) {
            $this->cleanupContentPackage($stalePackage);
        }
    }

    protected function getAllStalePackages(): array
    {
        $staleTime = strtotime('-1 day');
        $criteria = new Criteria();
        $criteria
            ->andWhere($criteria::expr()->lte('timestamp', $staleTime));

        return $this->statusRepository->matching($criteria)->toArray();
    }

    protected function cleanupContentPackage(ContentPackageStatus $contentPackageStatus)
    {
        $adminLogin = $contentPackageStatus->getUserId();
        $packageId = $contentPackageStatus->getPackageId();
        $exportPackageDir = $this->getUserPackageDirectory(
            $adminLogin,
            $packageId,
            false,
            false
        );
        $importPackageDir = $this->getUserPackageDirectory(
            $adminLogin,
            $packageId,
            false,
            true
        );
        $dirs = [$exportPackageDir, $importPackageDir];
        try {
            foreach ($dirs as $dir) {
                if (is_dir($dir) && file_exists($dir)) {
                    $this->clearDirectory($dir);
                    rmdir($dir);
                }
            }
            $this->removeStatus($contentPackageStatus);
        } catch (Exception $e) {
        }
    }

    public function getStatus(
        $adminLogin = null,
        $id = null
    ) : ContentPackageStatus {
        if ($adminLogin === null) {
            $adminLogin = $this->getAdminLogin();
        }
        if ($id === null) {
            $id = $this->getPackageId();
        }

        $entity = $this->statusRepository->findOneBy([
            'packageId' => $id,
            'userId' => $adminLogin,
        ]);

        if (!($entity instanceof ContentPackageStatus)) {
            throw new Exception(
                str_replace(
                    '$LOCATION_ID',
                    $id,
                    str_replace(
                        '$ADMIN_LOGIN',
                        $adminLogin,
                        self::ERROR_MESSAGES['content_package_missing']
                    )
                )
            );
        }

        return $entity;
    }

    public function updateStatus(ContentPackageStatus $entity, int $progress): void
    {
        $entity->setProgress($progress);
        $this->persistStatus($entity);
    }

    public function persistStatus(ContentPackageStatus $progress): void
    {
        if (!$this->entityManager->isOpen()) {
            $this->entityManager = $this->entityManager::create(
                $this->entityManager->getConnection(),
                $this->entityManager->getConfiguration()
            );
        }

        $this->entityManager->persist($progress);
        $this->entityManager->flush();
    }

    public function removeStatus(ContentPackageStatus $progress): void
    {
        $this->entityManager->remove($progress);
        $this->entityManager->flush();
    }

    public function setPackageId($packageId)
    {
        $this->packageId = $packageId;
    }

    public function getPackageId()
    {
        if ($this->packageId === null) {
            throw new Exception(self::ERROR_MESSAGES['package_id']);
        }

        return $this->packageId;
    }

    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

    public function getParentId()
    {
        if ($this->parentId === null) {
            throw new Exception(self::ERROR_MESSAGES['parent_id']);
        }

        return $this->parentId;
    }

    public function setType($type)
    {
        // TODO: validate
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setLanguage($language)
    {
        $this->language = $language;
    }

    public function getLanguage()
    {
        return $this->language;
    }

    public function setAdminLogin($adminLogin)
    {
        $this->adminLogin = $adminLogin;
    }

    public function getAdminLogin()
    {
        return $this->adminLogin;
    }

    protected function writeln($message)
    {
        if ($this->output) {
            $this->output->writeln($message);
        }
    }

    public function getBaseDirectory(
        $noRootDir = false
    ) {
        $d = $noRootDir ? '' : self::$PROJECT_ROOT_DIR . '/';
        $d .= self::$MIGRATION_FILE_DIRECTORY;

        return $d;
    }

    public function getImportDirectory(
        $noRootDir = false
    ) {
        $d = $this->getBaseDirectory($noRootDir);
        $d .= '/' . self::$RELATIVE_IMPORTS_DIR;

        return $d;
    }

    public function getExportDirectory(
        $noRootDir = false
    ) {
        $d = $this->getBaseDirectory($noRootDir);
        $d .= '/' . self::$RELATIVE_EXPORTS_DIR;

        return $d;
    }

    public function getUserDirectory(
        $adminLogin = null,
        $noRootDir = false,
        $import = false
    ) {
        if ($adminLogin === null) {
            $adminLogin = $this->getAdminLogin();
        }
        $d = $import ? $this->getImportDirectory($noRootDir) : $this->getExportDirectory($noRootDir);
        $d .= '/' . $adminLogin;

        return $d;
    }

    public function getUserPackageDirectory(
        $adminLogin = null,
        $packageId = null,
        $noRootDir = false,
        $import = false
    ) {
        if ($packageId === null) {
            $packageId = $this->getPackageId();
        }
        $d = $this->getUserDirectory($adminLogin, $noRootDir, $import);
        $d .= '/' . $packageId;

        return $d;
    }

    public function getFilesDirectory(
        $adminLogin = null,
        $packageId = null,
        $noRootDir = false,
        $import = false
    ) {
        $d = $this->getUserPackageDirectory($adminLogin, $packageId, $noRootDir, $import);
        $d .= '/files';

        return $d;
    }

    public function getImagesDirectory(
        $adminLogin = null,
        $packageId = null,
        $noRootDir = false,
        $import = false
    ) {
        $d = $this->getUserPackageDirectory($adminLogin, $packageId, $noRootDir, $import);
        $d .= '/images';

        return $d;
    }

    public function getMediaDirectory(
        $adminLogin = null,
        $packageId = null,
        $noRootDir = false,
        $import = false
    ) {
        $d = $this->getUserPackageDirectory($adminLogin, $packageId, $noRootDir, $import);
        $d .= '/media';

        return $d;
    }

    public function getLogFilepath(
        $adminLogin = null,
        $packageId = null,
        $noRootDir = false,
        $import = false
    ) {
        $d = $this->getUserPackageDirectory($adminLogin, $packageId, $noRootDir, $import);
        $d .= '/' . self::LOG_FILENAME;

        return $d;
    }

    protected function clearDirectory($path, $skipFilepaths = [], $skipFilenames = [])
    {
        $directoryIterator = new RecursiveDirectoryIterator(
            $path,
            RecursiveDirectoryIterator::SKIP_DOTS
        );
        $files = new RecursiveIteratorIterator(
            $directoryIterator,
            RecursiveIteratorIterator::CHILD_FIRST
        );
        foreach ($files as $file) {
            if (
                in_array($file->getPathname(), $skipFilepaths, true) ||
                in_array($file->getFilename(), $skipFilenames, true)
            ) {
                continue;
            }
            if ($file->getType() === 'dir') {
                rmdir($file->getPathname());
            } else {
                unlink($file->getPathname());
            }
        }
    }

    protected function ensureDirectoryExists($path, $mode = null, $levelsUp = 0)
    {
        if ($mode === null) {
            $mode = 0700;
        }
        if ($levelsUp) {
            $path = dirname($path, $levelsUp);
        }
        if (!is_dir($path)) {
            mkdir($path, $mode, true);
        }
    }

    public function setProjectRootDir(string $projectRootDir)
    {
        self::$PROJECT_ROOT_DIR = $projectRootDir;
    }

    public function getZipFilename(
        $packageId = null
    ) : string {
        if ($packageId === null) {
            $packageId = $this->getPackageId();
        }

        return 'content_package_' . $packageId . '.zip';
    }

    public function getZipFilepath(
        $adminLogin = null,
        $packageId = null,
        $noRootDir = false,
        $import = false
    ) {
        if ($adminLogin === null) {
            $adminLogin = $this->getAdminLogin();
        }
        if ($packageId === null) {
            $packageId = $this->getPackageId();
        }
        $d = $this->getUserPackageDirectory($adminLogin, $packageId, $noRootDir, $import);
        $d .= '/' . $this->getZipFilename($packageId);

        return $d;
    }

    public function getMetadataFile()
    {
        return self::METADATA_FILENAME;
    }

    public function getMetadataFilepath(
        $adminLogin = null,
        $packageId = null,
        $noRootDir = false,
        $import = false
    ) : string {
        if ($adminLogin === null) {
            $adminLogin = $this->getAdminLogin();
        }
        if ($packageId === null) {
            $packageId = $this->getPackageId();
        }
        $d = $this->getUserPackageDirectory($adminLogin, $packageId, $noRootDir, $import);
        $d .= '/' . $this->getMetadataFile();

        return $d;
    }

    protected function createMetadataFile(
        string $name,
        string $adminLogin,
        string $packageId,
        bool $import,
        array $extraData = []
    ) {
        $metadataFilepath = $this->getMetadataFilepath($adminLogin, $packageId, false, $import);
        $metadata = $this->createMetadata($name, $adminLogin, $packageId, $extraData);
        $yaml = Yaml::dump($metadata->getItems(), 4, 4);
        file_put_contents($metadataFilepath, $yaml);
    }

    protected function createMetadata(
        string $name,
        string $adminLogin,
        string $packageId,
        array $extraData
    ) : ContentPackageMetadata {
        $status = $this->getStatus($adminLogin, $packageId);
        $date = $status->getDate();
        $dateFormatted = $date->format(self::$METADATA_DATE_FORMAT);

        return new ContentPackageMetadata(
            array_merge(
                [
                    'Name' => $name,
                    'Type' => $this->getType(),
                    'Admin Login' => $adminLogin,
                    'Package Id' => $packageId,
                    'Created On' => $dateFormatted,
                    'Content Package Version' => self::VERSION,
                ],
                $extraData
            )
        );
    }

    public function getMetadata(
        $adminLogin = null,
        $packageId = null,
        $noRootDir = false,
        $import = false
    ) : ContentPackageMetadata {
        $filepath = $this->getMetadataFilepath($adminLogin, $packageId, $noRootDir, $import);
        try {
            $data = Yaml::parseFile($filepath);
        } catch (Exception $e) {
            $data = [];
        }
        return new ContentPackageMetadata($data);
    }

    protected static function getLocationReferenceKey($id): string
    {
        return 'location_' . $id . '_new_location_id';
    }

    protected static function getContentReferenceKey($id): string
    {
        return 'location_' . $id . '_new_content_id';
    }

    protected static function getContentRemoteIdReferenceKey($id): string
    {
        return 'location_' . $id . '_new_content_remote_id';
    }

    protected static function getTagRemoteIdReferenceKey($id): string
    {
        return 'tag_' . $id . '_new_remote_id';
    }
}
