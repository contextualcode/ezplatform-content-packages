<?php

/**
 * This is an override of eZ\Publish\Core\Repository\Repository\Repository.php
 * in order to use our ContentService override.
 */

namespace ContextualCode\EzPlatformContentPackagesBundle\Service\Override;

use ContextualCode\EzPlatformContentPackagesBundle\Service\Override\ContentService as OverrideContentService;
use eZ\Publish\Core\Repository\Repository as BaseRepository;

/**
 * Repository class.
 */
class Repository extends BaseRepository
{
    /**
     * @inheritDoc
     */
    public function getContentService()
    {
        if ($this->contentService !== null) {
            return $this->contentService;
        }

        $this->contentService = new OverrideContentService(
            $this,
            $this->persistenceHandler,
            $this->getDomainMapper(),
            $this->getRelationProcessor(),
            $this->getNameSchemaService(),
            $this->getFieldTypeRegistry(),
            $this->serviceSettings['content']
        );

        return $this->contentService;
    }
}
