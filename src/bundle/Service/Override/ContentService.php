<?php

/**
 * This is an override of eZ's ContentService
 * in order to get past validation errors on creating/updating content
 *
 * See $ignoreFieldValidationErrors usage
 */

namespace ContextualCode\EzPlatformContentPackagesBundle\Service\Override;

use Exception;
use eZ\Publish\API\Repository\Exceptions\ContentValidationException;
use eZ\Publish\API\Repository\Exceptions\NotFoundException as APINotFoundException;
use eZ\Publish\API\Repository\Values\Content\ContentCreateStruct as APIContentCreateStruct;
use eZ\Publish\API\Repository\Values\Content\ContentUpdateStruct as APIContentUpdateStruct;
use eZ\Publish\API\Repository\Values\Content\LocationCreateStruct;
use eZ\Publish\API\Repository\Values\Content\VersionInfo as APIVersionInfo;
use eZ\Publish\Core\Base\Exceptions\BadStateException;
use eZ\Publish\Core\Base\Exceptions\ContentFieldValidationException;
use eZ\Publish\Core\Base\Exceptions\InvalidArgumentException;
use eZ\Publish\Core\Base\Exceptions\UnauthorizedException;
use eZ\Publish\Core\FieldType\ValidationError;
use eZ\Publish\Core\Repository\ContentService as BaseContentService;
use eZ\Publish\Core\Repository\Values\Content\Content;
use eZ\Publish\SPI\FieldType\FieldType;
use eZ\Publish\SPI\Limitation\Target;
use eZ\Publish\SPI\Persistence\Content\CreateStruct as SPIContentCreateStruct;
use eZ\Publish\SPI\Persistence\Content\Field as SPIField;
use eZ\Publish\SPI\Persistence\Content\UpdateStruct as SPIContentUpdateStruct;

/**
 * {@inheritDoc}
 */
class ContentService extends BaseContentService
{
    /** @var bool */
    protected static $ignoreFieldValidationErrors = false;

    public function setIgnoreFieldValidationErrors(bool $value): void
    {
        self::$ignoreFieldValidationErrors = $value;
    }

    public function getIgnoreFieldValidationErrors(): bool
    {
        return self::$ignoreFieldValidationErrors;
    }

    /**
     * Creates a new content draft assigned to the authenticated user.
     *
     * If a different userId is given in $contentCreateStruct it is assigned to the given user
     * but this required special rights for the authenticated user
     * (this is useful for content staging where the transfer process does not
     * have to authenticate with the user which created the content object in the source server).
     * The user has to publish the draft if it should be visible.
     * In 4.x at least one location has to be provided in the location creation array.
     *
     * @param APIContentCreateStruct $contentCreateStruct
     * @param LocationCreateStruct[] $locationCreateStructs For each location parent under which a location should be created for the content
     *
     * @return \eZ\Publish\API\Repository\Values\Content\Content - the newly created content draft
     * @throws ContentValidationException If field definition does not exist in the ContentType,
     *                                                                          or value is set for non-translatable field in language
     *                                                                          other than main.
     *
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException if the user is not allowed to create the content in the given location
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException if the provided remoteId exists in the system, required properties on
     *                                                                        struct are missing or invalid, or if multiple locations are under the
     *                                                                        same parent.
     * @throws \eZ\Publish\API\Repository\Exceptions\ContentFieldValidationException if a field in the $contentCreateStruct is not valid,
     *                                                                               or if a required field is missing / set to an empty value.
     */
    public function createContent(APIContentCreateStruct $contentCreateStruct, array $locationCreateStructs = [])
    {
        if (!$this->getIgnoreFieldValidationErrors()) {
            return parent::createContent(
                $contentCreateStruct,
                $locationCreateStructs
            );
        }

        if ($contentCreateStruct->mainLanguageCode === null) {
            throw new InvalidArgumentException('$contentCreateStruct', "'mainLanguageCode' property must be set");
        }

        if ($contentCreateStruct->contentType === null) {
            throw new InvalidArgumentException('$contentCreateStruct', "'contentType' property must be set");
        }

        $contentCreateStruct = clone $contentCreateStruct;

        if ($contentCreateStruct->ownerId === null) {
            $contentCreateStruct->ownerId = $this->repository->getCurrentUserReference()->getUserId();
        }

        if ($contentCreateStruct->alwaysAvailable === null) {
            $contentCreateStruct->alwaysAvailable = $contentCreateStruct->contentType->defaultAlwaysAvailable ?: false;
        }

        $contentCreateStruct->contentType = $this->repository->getContentTypeService()->loadContentType(
            $contentCreateStruct->contentType->id
        );

        if (empty($contentCreateStruct->sectionId)) {
            if (isset($locationCreateStructs[0])) {
                $location = $this->repository->getLocationService()->loadLocation(
                    $locationCreateStructs[0]->parentLocationId
                );
                $contentCreateStruct->sectionId = $location->contentInfo->sectionId;
            } else {
                $contentCreateStruct->sectionId = 1;
            }
        }

        if (!$this->repository->canUser('content', 'create', $contentCreateStruct, $locationCreateStructs)) {
            throw new UnauthorizedException(
                'content',
                'create',
                [
                    'parentLocationId' => isset($locationCreateStructs[0]) ?
                        $locationCreateStructs[0]->parentLocationId :
                        null,
                    'sectionId' => $contentCreateStruct->sectionId,
                ]
            );
        }

        if (!empty($contentCreateStruct->remoteId)) {
            try {
                $this->loadContentByRemoteId($contentCreateStruct->remoteId);

                throw new InvalidArgumentException(
                    '$contentCreateStruct',
                    "Another content with remoteId '{$contentCreateStruct->remoteId}' exists"
                );
            } catch (APINotFoundException $e) {
                // Do nothing
            }
        } else {
            $contentCreateStruct->remoteId = $this->domainMapper->getUniqueHash($contentCreateStruct);
        }

        $spiLocationCreateStructs = $this->buildSPILocationCreateStructs($locationCreateStructs);

        $languageCodes = $this->getLanguageCodesForCreate($contentCreateStruct);
        $fields = $this->mapFieldsForCreate($contentCreateStruct);

        $fieldValues = [];
        $spiFields = [];
        $allFieldErrors = [];
        $inputRelations = [];
        $locationIdToContentIdMapping = [];

        foreach ($contentCreateStruct->contentType->getFieldDefinitions() as $fieldDefinition) {
            /** @var $fieldType \eZ\Publish\Core\FieldType\FieldType */
            $fieldType = $this->fieldTypeRegistry->getFieldType(
                $fieldDefinition->fieldTypeIdentifier
            );

            foreach ($languageCodes as $languageCode) {
                $isEmptyValue = false;
                $valueLanguageCode = $fieldDefinition->isTranslatable ? $languageCode : $contentCreateStruct->mainLanguageCode;
                $isLanguageMain = $languageCode === $contentCreateStruct->mainLanguageCode;
                if (isset($fields[$fieldDefinition->identifier][$valueLanguageCode])) {
                    $fieldValue = $fields[$fieldDefinition->identifier][$valueLanguageCode]->value;
                } else {
                    $fieldValue = $fieldDefinition->defaultValue;
                }

                $fieldValue = $fieldType->acceptValue($fieldValue);
                if ($fieldType->isEmptyValue($fieldValue)) {
                    $isEmptyValue = true;
                    if ($fieldDefinition->isRequired) {
                        $allFieldErrors[$fieldDefinition->id][$languageCode] = new ValidationError(
                            "Value for required field definition '%identifier%' with language '%languageCode%' is empty",
                            null,
                            ['%identifier%' => $fieldDefinition->identifier, '%languageCode%' => $languageCode],
                            'empty'
                        );
                    }
                } else {
                    $fieldErrors = $fieldType->validate(
                        $fieldDefinition,
                        $fieldValue
                    );
                    if (!empty($fieldErrors)) {
                        $allFieldErrors[$fieldDefinition->id][$languageCode] = $fieldErrors;
                    }
                }

                if (!empty($allFieldErrors) && !$this->getIgnoreFieldValidationErrors()) {
                    continue;
                }

                $this->relationProcessor->appendFieldRelations(
                    $inputRelations,
                    $locationIdToContentIdMapping,
                    $fieldType,
                    $fieldValue,
                    $fieldDefinition->id
                );
                $fieldValues[$fieldDefinition->identifier][$languageCode] = $fieldValue;

                // Only non-empty value for: translatable field or in main language
                if (
                    (!$isEmptyValue && $fieldDefinition->isTranslatable) ||
                    (!$isEmptyValue && $isLanguageMain)
                ) {
                    $spiFields[] = new SPIField(
                        [
                            'id' => null,
                            'fieldDefinitionId' => $fieldDefinition->id,
                            'type' => $fieldDefinition->fieldTypeIdentifier,
                            'value' => $fieldType->toPersistenceValue($fieldValue),
                            'languageCode' => $languageCode,
                            'versionNo' => null,
                        ]
                    );
                }
            }
        }

        if ($allFieldErrors && !$this->getIgnoreFieldValidationErrors()) {
            throw new ContentFieldValidationException($allFieldErrors);
        }

        $spiContentCreateStruct = new SPIContentCreateStruct(
            [
                'name' => $this->nameSchemaService->resolve(
                    $contentCreateStruct->contentType->nameSchema,
                    $contentCreateStruct->contentType,
                    $fieldValues,
                    $languageCodes
                ),
                'typeId' => $contentCreateStruct->contentType->id,
                'sectionId' => $contentCreateStruct->sectionId,
                'ownerId' => $contentCreateStruct->ownerId,
                'locations' => $spiLocationCreateStructs,
                'fields' => $spiFields,
                'alwaysAvailable' => $contentCreateStruct->alwaysAvailable,
                'remoteId' => $contentCreateStruct->remoteId,
                'modified' => isset($contentCreateStruct->modificationDate) ? $contentCreateStruct->modificationDate->getTimestamp() : time(),
                'initialLanguageId' => $this->persistenceHandler->contentLanguageHandler()->loadByLanguageCode(
                    $contentCreateStruct->mainLanguageCode
                )->id,
            ]
        );

        $defaultObjectStates = $this->getDefaultObjectStates();

        $this->repository->beginTransaction();
        try {
            $spiContent = $this->persistenceHandler->contentHandler()->create($spiContentCreateStruct);
            $this->relationProcessor->processFieldRelations(
                $inputRelations,
                $spiContent->versionInfo->contentInfo->id,
                $spiContent->versionInfo->versionNo,
                $contentCreateStruct->contentType
            );

            $objectStateHandler = $this->persistenceHandler->objectStateHandler();
            foreach ($defaultObjectStates as $objectStateGroupId => $objectState) {
                $objectStateHandler->setContentState(
                    $spiContent->versionInfo->contentInfo->id,
                    $objectStateGroupId,
                    $objectState->id
                );
            }

            $this->repository->commit();
        } catch (Exception $e) {
            $this->repository->rollback();
            throw $e;
        }

        return $this->domainMapper->buildContentDomainObject(
            $spiContent,
            $contentCreateStruct->contentType
        );
    }

    /**
     * Updates the fields of a draft.
     *
     * @param APIVersionInfo $versionInfo
     * @param APIContentUpdateStruct $contentUpdateStruct
     *
     * @return \eZ\Publish\API\Repository\Values\Content\Content the content draft with the updated fields
     *
     * @throws \eZ\Publish\API\Repository\Exceptions\ContentFieldValidationException if a field in the $contentCreateStruct is not valid,
     *                                                                               or if a required field is missing / set to an empty value.
     * @throws ContentValidationException If field definition does not exist in the ContentType,
     *                                                                          or value is set for non-translatable field in language
     *                                                                          other than main.
     *
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException if the user is not allowed to update this version
     * @throws \eZ\Publish\API\Repository\Exceptions\BadStateException if the version is not a draft
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException if a property on the struct is invalid.
     * @throws APINotFoundException
     */
    public function updateContent(APIVersionInfo $versionInfo, APIContentUpdateStruct $contentUpdateStruct)
    {
        if (!$this->getIgnoreFieldValidationErrors()) {
            return parent::updateContent(
                $versionInfo,
                $contentUpdateStruct
            );
        }

        $contentUpdateStruct = clone $contentUpdateStruct;

        /** @var $content Content */
        $content = $this->loadContent(
            $versionInfo->getContentInfo()->id,
            null,
            $versionInfo->versionNo
        );
        if (!$content->versionInfo->isDraft()) {
            throw new BadStateException(
                '$versionInfo',
                'Version is not a draft and can not be updated'
            );
        }

        if (!$this->repository->getPermissionResolver()->canUser(
            'content',
            'edit',
            $content,
            [
                (new Target\Builder\VersionBuilder())
                    ->updateFieldsTo(
                        $contentUpdateStruct->initialLanguageCode,
                        $contentUpdateStruct->fields
                    )
                    ->build(),
            ]
        )) {
            throw new UnauthorizedException('content', 'edit', ['contentId' => $content->id]);
        }

        $mainLanguageCode = $content->contentInfo->mainLanguageCode;
        if ($contentUpdateStruct->initialLanguageCode === null) {
            $contentUpdateStruct->initialLanguageCode = $mainLanguageCode;
        }

        $allLanguageCodes = $this->getLanguageCodesForUpdate($contentUpdateStruct, $content);
        $contentLanguageHandler = $this->persistenceHandler->contentLanguageHandler();
        foreach ($allLanguageCodes as $languageCode) {
            $contentLanguageHandler->loadByLanguageCode($languageCode);
        }

        $updatedLanguageCodes = $this->getUpdatedLanguageCodes($contentUpdateStruct);
        $contentType = $this->repository->getContentTypeService()->loadContentType(
            $content->contentInfo->contentTypeId
        );
        $fields = $this->mapFieldsForUpdate(
            $contentUpdateStruct,
            $contentType,
            $mainLanguageCode
        );

        $fieldValues = [];
        $spiFields = [];
        $allFieldErrors = [];
        $inputRelations = [];
        $locationIdToContentIdMapping = [];

        foreach ($contentType->getFieldDefinitions() as $fieldDefinition) {
            /** @var $fieldType FieldType */
            $fieldType = $this->fieldTypeRegistry->getFieldType(
                $fieldDefinition->fieldTypeIdentifier
            );

            foreach ($allLanguageCodes as $languageCode) {
                $isCopied = $isEmpty = $isRetained = false;
                $isLanguageNew = !in_array($languageCode, $content->versionInfo->languageCodes);
                $isLanguageUpdated = in_array($languageCode, $updatedLanguageCodes);
                $valueLanguageCode = $fieldDefinition->isTranslatable ? $languageCode : $mainLanguageCode;
                $isFieldUpdated = isset($fields[$fieldDefinition->identifier][$valueLanguageCode]);
                $isProcessed = isset($fieldValues[$fieldDefinition->identifier][$valueLanguageCode]);

                if (!$isFieldUpdated && !$isLanguageNew) {
                    $isRetained = true;
                    $fieldValue = $content->getField($fieldDefinition->identifier, $valueLanguageCode)->value;
                } elseif (!$isFieldUpdated && $isLanguageNew && !$fieldDefinition->isTranslatable) {
                    $isCopied = true;
                    $fieldValue = $content->getField($fieldDefinition->identifier, $valueLanguageCode)->value;
                } elseif ($isFieldUpdated) {
                    $fieldValue = $fields[$fieldDefinition->identifier][$valueLanguageCode]->value;
                } else {
                    $fieldValue = $fieldDefinition->defaultValue;
                }

                $fieldValue = $fieldType->acceptValue($fieldValue);

                if ($fieldType->isEmptyValue($fieldValue)) {
                    $isEmpty = true;
                    if ($isLanguageUpdated && $fieldDefinition->isRequired) {
                        $allFieldErrors[$fieldDefinition->id][$languageCode] = new ValidationError(
                            "Value for required field definition '%identifier%' with language '%languageCode%' is empty",
                            null,
                            ['%identifier%' => $fieldDefinition->identifier, '%languageCode%' => $languageCode],
                            'empty'
                        );
                    }
                } elseif ($isLanguageUpdated) {
                    $fieldErrors = $fieldType->validate(
                        $fieldDefinition,
                        $fieldValue
                    );
                    if (!empty($fieldErrors)) {
                        $allFieldErrors[$fieldDefinition->id][$languageCode] = $fieldErrors;
                    }
                }

                if (!empty($allFieldErrors) && !$this->getIgnoreFieldValidationErrors()) {
                    continue;
                }

                $this->relationProcessor->appendFieldRelations(
                    $inputRelations,
                    $locationIdToContentIdMapping,
                    $fieldType,
                    $fieldValue,
                    $fieldDefinition->id
                );
                $fieldValues[$fieldDefinition->identifier][$languageCode] = $fieldValue;

                if ($isRetained || $isCopied || ($isLanguageNew && $isEmpty) || $isProcessed) {
                    continue;
                }

                $spiFields[] = new SPIField(
                    [
                        'id' => $isLanguageNew ?
                            null :
                            $content->getField($fieldDefinition->identifier, $languageCode)->id,
                        'fieldDefinitionId' => $fieldDefinition->id,
                        'type' => $fieldDefinition->fieldTypeIdentifier,
                        'value' => $fieldType->toPersistenceValue($fieldValue),
                        'languageCode' => $languageCode,
                        'versionNo' => $versionInfo->versionNo,
                    ]
                );
            }
        }

        if ($allFieldErrors && !$this->getIgnoreFieldValidationErrors()) {
            throw new ContentFieldValidationException($allFieldErrors);
        }

        $spiContentUpdateStruct = new SPIContentUpdateStruct(
            [
                'name' => $this->nameSchemaService->resolveNameSchema(
                    $content,
                    $fieldValues,
                    $allLanguageCodes,
                    $contentType
                ),
                'creatorId' => $contentUpdateStruct->creatorId ?: $this->repository->getCurrentUserReference()->getUserId(),
                'fields' => $spiFields,
                'modificationDate' => time(),
                'initialLanguageId' => $this->persistenceHandler->contentLanguageHandler()->loadByLanguageCode(
                    $contentUpdateStruct->initialLanguageCode
                )->id,
            ]
        );
        $existingRelations = $this->loadRelations($versionInfo);

        $this->repository->beginTransaction();
        try {
            $spiContent = $this->persistenceHandler->contentHandler()->updateContent(
                $versionInfo->getContentInfo()->id,
                $versionInfo->versionNo,
                $spiContentUpdateStruct
            );
            $this->relationProcessor->processFieldRelations(
                $inputRelations,
                $spiContent->versionInfo->contentInfo->id,
                $spiContent->versionInfo->versionNo,
                $contentType,
                $existingRelations
            );
            $this->repository->commit();
        } catch (Exception $e) {
            $this->repository->rollback();
            throw $e;
        }

        return $this->domainMapper->buildContentDomainObject(
            $spiContent,
            $contentType
        );
    }

    /**
     * Returns only updated language codes.
     *
     * @param APIContentUpdateStruct $contentUpdateStruct
     *
     * @return array
     */
    private function getUpdatedLanguageCodes(APIContentUpdateStruct $contentUpdateStruct)
    {
        $languageCodes = [
            $contentUpdateStruct->initialLanguageCode => true,
        ];

        foreach ($contentUpdateStruct->fields as $field) {
            if ($field->languageCode === null || isset($languageCodes[$field->languageCode])) {
                continue;
            }

            $languageCodes[$field->languageCode] = true;
        }

        return array_keys($languageCodes);
    }
}
