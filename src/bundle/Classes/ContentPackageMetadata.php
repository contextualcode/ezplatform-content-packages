<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Classes;

class ContentPackageMetadata
{
    /** @var array */
    protected $items = [];

    public function __construct(
        array $items = []
    ) {
        $this->addItems($items);
    }

    public function addItems(array $items)
    {
        foreach ($items as $key => $value) {
            $this->addItem($key, $value);
        }
    }

    public function addItem(string $key, $value)
    {
        $this->items[$key] = $value;
    }

    public function getItems() : array
    {
        return $this->items;
    }
}
