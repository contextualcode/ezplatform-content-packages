CREATE TABLE `cc_content_packages_status`
(
    `package_id`  varchar(255)     NOT NULL,
    `user_id`     varchar(255)     NOT NULL,
    `timestamp`   int(255) unsigned,
    `in_progress` boolean,
    `progress`    int(11) unsigned NOT NULL,
    `error`       varchar(255)     NOT NULL,
    PRIMARY KEY (`package_id`, `user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
