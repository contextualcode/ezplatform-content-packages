<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\FilterIterator;

use ContextualCode\EzPlatformContentPackagesBundle\Service\ContentPackagesService;
use RecursiveFilterIterator;

class MigrateFilesRecursiveFilterIterator extends RecursiveFilterIterator
{
    public static $EXCLUDE_FILENAMES = [
        ContentPackagesService::METADATA_FILENAME,
    ];

    public static $YML_EXTENSIONS = [
        'yml',
        'yaml',
    ];

    public function accept()
    {
        if (
            in_array(
                $this->current()->getFilename(),
                self::$EXCLUDE_FILENAMES,
                true
            )
        ) {
            return false;
        }

        return in_array(
            $this->current()->getExtension(),
            self::$YML_EXTENSIONS,
            true
        );
    }
}
