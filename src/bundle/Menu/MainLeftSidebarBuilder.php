<?php

namespace ContextualCode\EzPlatformContentPackagesBundle\Menu;

use EzSystems\EzPlatformAdminUi\Menu\AbstractBuilder;
use Knp\Menu\ItemInterface;

class MainLeftSidebarBuilder extends AbstractBuilder
{
    public const ITEM__DASHBOARD = 'content_packages_main__sidebar_left__dashboard';
    public const ITEM__EXPORT = 'content_packages_main__sidebar_left__export';
    public const ITEM__IMPORT = 'content_packages_main__sidebar_left__import';
    public const ITEM__ABOUT = 'content_packages_main__sidebar_left__about';

    protected function getConfigureEventName(): string
    {
        return 'content_packages.menu_configure.main_sidebar_left';
    }

    protected function createStructure(array $options): ItemInterface
    {
        /** @var ItemInterface $menu */
        $menu = $this->factory->createItem('root');

        $menu->setChildren([
            self::ITEM__DASHBOARD => $this->createMenuItem(
                'Dashboard',
                [
                    'route' => 'contextual_code_content_packages_dashboard',
                    'extras' => ['icon' => 'contentlist'],
                ]
            ),
            self::ITEM__EXPORT => $this->createMenuItem(
                'Export',
                [
                    'route' => 'contextual_code_content_packages_export',
                    'extras' => ['icon' => 'download'],
                ]
            ),
            self::ITEM__IMPORT => $this->createMenuItem(
                'Import',
                [
                    'route' => 'contextual_code_content_packages_import',
                    'extras' => ['icon' => 'upload'],
                ]
            ),
            self::ITEM__ABOUT => $this->createMenuItem(
                'About',
                [
                    'route' => 'contextual_code_content_packages_about',
                    'extras' => ['icon' => 'about-info'],
                ]
            ),
        ]);

        return $menu;
    }
}
